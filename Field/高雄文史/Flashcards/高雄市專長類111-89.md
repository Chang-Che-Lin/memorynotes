---
Type: flashcard
View: Back
---
Level:: 0%%0%%
Next Learn Date:: 2023-05-18%%2023-05-18%%

# Front
Front:: 89.「亞洲新灣區」，以寬廣的高雄港灣為基地，在高雄港 15 至 21 號碼頭及 周邊啟動一系列的城市建設計畫，以下何者是在這一系列的城市建設計畫 中?
Front:: (A) 高雄世界貿易展覽會議中心
Front:: (B) 大林浦南星計畫
Front:: (C) 高雄市漁貨產業館
Front:: (D) 情人碼頭

# Back
Back:: (A) 高雄世界貿易展覽會議中心

## Result
Finish:: `$= const {MemoryApi} = customJS; MemoryApi.showAnswerState(this, dv.current())? "✅": "🟩"`
```dataviewjs
const {MemoryApi} = customJS

MemoryApi.flashcardSpacedRepetition(this)
```

## Reference
Range:: ==請自行填入範圍 (ex：[[地理#Header1]] )==
