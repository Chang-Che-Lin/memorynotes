---
Type: flashcard
View: Back
---
Level:: 0%%0%%
Next Learn Date:: 2023-05-18%%2023-05-18%%

# Front
Front:: 71.族群接觸往往導致文化變遷。歷史文獻紀錄，今內門、田寮地區是西拉雅 族新港社的移住區，祀壺信仰是該族群的文化特徵，而且會有奉祀太祖的 公廨。試問：內門、田寮地區的公廨信仰在與漢人接觸後多數轉變那一種形式？
Front:: (A) 萬善爺信仰
Front:: (B) 媽娘信仰
Front:: (C) 玄天上帝信仰 
Front:: (D) 地基主信仰

# Back
Back:: (D) 地基主信仰

## Result
Finish:: `$= const {MemoryApi} = customJS; MemoryApi.showAnswerState(this, dv.current())? "✅": "🟩"`
```dataviewjs
const {MemoryApi} = customJS

MemoryApi.flashcardSpacedRepetition(this)
dv.view("Settings")
```

## Reference
Range:: [[地理#Header1]]
