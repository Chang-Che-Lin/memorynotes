---
Type: Deck
Flashcards Folder: Flashcards
---
```dataviewjs
const {MemoryApi} = customJS

MemoryApi.showProgress(this)
MemoryApi.makeNewTemplate(this, "TemplateFlashcard", "Add Flashcard")
```

```dataviewjs
const {MemoryApi} = customJS

MemoryApi.showFlashcard(this)
dv.view("Settings")
```
