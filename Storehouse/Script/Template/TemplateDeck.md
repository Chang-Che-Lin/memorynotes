<%*
const deckFolderName = await tp.system.prompt("Deck folder name")
const deckFolderPath = await this.app.workspace.getActiveFile().parent.path + "/" + deckFolderName
await this.app.vault.createFolder(deckFolderPath)

const flashcardsFolderName = await tp.system.prompt("Flashcards folder name (default：Flashcards)", "Flashcards")
const flashcardsFolderPath = deckFolderPath + "/" + flashcardsFolderName
await this.app.vault.createFolder(flashcardsFolderPath)

const fileName = "Deck"
await tp.file.move(deckFolderPath + "/" + fileName)

tR += "---\n"
tR += "Type: Deck\n"
tR += "Flashcards Folder: "
tR += flashcardsFolderName
tR += "\n---"
%>
```dataviewjs
const {MemoryApi} = customJS

MemoryApi.showProgress(this)
MemoryApi.makeNewTemplate(this, "TemplateFlashcard", "Add Flashcard")
```

```dataviewjs
const {MemoryApi} = customJS

MemoryApi.showFlashcard(this)
dv.view("Settings")
```
