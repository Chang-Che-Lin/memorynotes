<%*
const dv = this.app.plugins.plugins["dataview"].api
const flashcardFolderName = dv.page(this.app.workspace.getActiveFile().path)["Flashcards Folder"]
const flashcardFolderPath = this.app.workspace.getActiveFile().parent.path + "/" + flashcardFolderName
const fileName = await tp.system.prompt("檔案名稱")
await tp.file.move(flashcardFolderPath + "/" + fileName)

tR += "---\n"
tR += "Type: flashcard\n"
tR += "View: Back\n"
tR += "---"
%>
Level:: 0%%0%%
Next Learn Date:: <% tp.date.now("YYYY-MM-DD") %>%%<% tp.date.now("YYYY-MM-DD") %>%%

# Front
<%*
const front = []

do {
	front[front.length] = await tp.system.prompt("Front")
} while (front[front.length - 1] != "" && front[front.length - 1] != null && front[front.length - 1] != undefined)
front.pop()
-%>
<%*
let frontItem = 0

do {
	tR += "Front:: "
	tR += front[frontItem]
	tR += "\n"
	frontItem += 1
} while (frontItem < front.length)
-%>

# Back
<%*
const back = []

do {
	back[back.length] = await tp.system.prompt("Back")
} while (back[back.length - 1] != "" && back[back.length - 1] != null && back[back.length - 1] != undefined)
back.pop()
-%>
<%*
let backItem = 0

do {
	tR += "Back:: "
	tR += back[backItem]
	tR += "\n"
	backItem += 1
} while (backItem < back.length)
-%>

## Result
Finish:: `$= const {MemoryApi} = customJS; MemoryApi.showAnswerState(this, dv.current())? "✅": "🟩"`
```dataviewjs
const {MemoryApi} = customJS

MemoryApi.flashcardSpacedRepetition(this)
```

## Reference
Range:: ==請自行填入範圍 (ex：[[地理#Header1]] )==
