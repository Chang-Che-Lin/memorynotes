class MemoryFunc {
  // count occurrence of dictionary key
  countRepeat(item, dictionary) {
    if (item in dictionary)
      dictionary[item]++
    else
      dictionary[item] = 1
  }

  // sort dictionary by value
  sortDictionary(dictionary) {
    var items = Object.keys(dictionary).map(
      (key) => { return [key, dictionary[key]] }
    )
    items.sort(
      (first, second) => { return second[1] - first[1] }
    )
    var keys = items.map(
      (e) => { return [e[0], e[1]] }
    )
    return keys
  }

  // count dictionary key occurrence rate, the output has been rounded
  countOccurrenceRate(dictionary, sum) {
    let occurrenceRate = {}

    Object.keys(dictionary).forEach(key => {
      occurrenceRate[key] = Math.round(dictionary[key] / sum * 100)
    })

    return occurrenceRate
  }

  // rank of occurrence rate by dictionary value
  rankOccurrenceRate(dictionary) {
    let rankDictionary = {}
    let rankValue = 0
    let dictValue = 0

    let sortedDictionary = this.sortDictionary(dictionary)

    for (var i = 0; i < sortedDictionary.length; i++) {
      if (sortedDictionary[i][1] != dictValue) {
        dictValue = sortedDictionary[i][1]
        rankValue++
        rankDictionary[sortedDictionary[i][0]] = rankValue
      }
      else {
        rankDictionary[sortedDictionary[i][0]] = rankValue
      }
    }

    return rankDictionary
  }

  // use luxon function: DateTime to calculate date
  offsetDate(dv, date, offset) {
    date = dv.luxon.DateTime.fromISO(date).plus({ days: offset }).toFormat('yyyy-MM-dd')
    return date
  }

  // up a level
  levelUp(level, maxLevel) {
    let lvUp = level

    if (lvUp < maxLevel)
      lvUp++
    else
      lvUp = maxLevel

    return lvUp
  }

  // reset level to 0
  levelReset() {
    return 0
  }

  // compare date. if dateA > dateB, return 1. if dateA = dateB, return 0. if dateA < dateB, return -1. 
  compareDate(dv, dateA, dateB) {
    return dv.compare(dv.luxon.DateTime.fromISO(dateA), dv.luxon.DateTime.fromISO(dateB))
  }

  // answer state
  answerState(dv, current) {
    const today = dv.luxon.DateTime.now().toFormat('yyyy-MM-dd')
    const currentLearnDate = current["Next Learn Date"]?.split("%%")[0]
    const pastLearnDate = current["Next Learn Date"]?.split("%%")[1]

    return (this.compareDate(dv, pastLearnDate, today) == 0 && this.compareDate(dv, currentLearnDate, pastLearnDate) != 0)
  }

  // count today finish flashcards
  countTodayFinish(dv, pages) {
    let count = 0

    pages.forEach(p => {
      if (this.answerState(dv, p))
        count++
    })

    return count
  }

  // count have been finished flashcards
  countHaveFinished(dv, pages) {
    let count = 0
    const today = dv.luxon.DateTime.now().toFormat('yyyy-MM-dd')

    pages.forEach(p => {
      if (this.compareDate(dv, p["Next Learn Date"]?.split("%%")[0], today) == 1)
        count++
    })

    return count
  }

  // total level
  countTotalLevel(pages, settingPages) {
    let count = pages.length * settingPages["Level MAX"]

    return count
  }

  // get level integer
  getLevel(page) {
    return parseInt(page.level?.split("%%")[0])
  }

  // add up current level
  countCurrentLevel(pages) {
    let count = 0

    pages.forEach(p => {
      count += this.getLevel(p)
    })

    return count
  }

  // Round to the second decimal place
  roundToTwo(num) {
    return +(Math.round(num + "e+2") + "e-2")
  }

  // show progress bar
  progressBar(dv, current, total) {
    let bar = `<progress style="height:20px;width:120px;vertical-align:middle;" value="`
      + current
      + `" max="`
      + total
      + `"></progress>`
      + "&nbsp&nbsp&nbsp"
      + this.roundToTwo(current / total * 100)
      + "%"
    return dv.paragraph(bar)
  }

  async updateDataview(dv, TFile) {
    await dv.index.persister.allFiles()
    if (TFile != null && TFile != undefined) {
      await dv.index.reload(TFile)
    }
    await dv.app.workspace.trigger("dataview:refresh-views")
  }

  getFlashcardsFolderPath(dv) {
    const folderName = dv.page(app.workspace.getActiveFile().path)["Flashcards Folder"]
    const dataPagesPath = `"` + dv.io.normalize((folderName === "") ? "Flashcards" : folderName) + `"`
    return dataPagesPath
  }

  reflashView(dv) {
    const { createButton } = app.plugins.plugins["buttons"]
    const addNew = async (file) => {
      await dv.app.workspace.trigger("dataview:refresh-views")
    }

    dv.array([
      createButton({
        app, el: dv.container,
        args: { name: "Reflash" },
        clickOverride: { click: addNew, params: [dv.current().file.path] }
      })
    ])
  }

  addNewFile(dv, template) {
    const { createButton } = app.plugins.plugins["buttons"]

    const addNew = async () => {
      //const filePath = dv.app.workspace.getActiveFile().parent.path + "/" + "test.md"
      //const file = await dv.app.vault.create(filePath, "")
      //const folderPath = await dv.app.workspace.getActiveFile().parent.path
      //const folder = app.vault.getAbstractFileByPath(folderPath)
      const tp = app.plugins.plugins['templater-obsidian'].templater.functions_generator.internal_functions
      const specifyTemplate = await tp.modules_array[1].static_functions.get('find_tfile')(template)
      newFile = await tp.modules_array[1].static_functions.get('create_new')(specifyTemplate)

      this.updateDataview(dv, newFile)
    }

    dv.array([
      createButton({
        app, el: dv.container,
        args: { name: "Add New" },
        clickOverride: { click: addNew, params: [] }
      })
    ])
  }

  tagColorForNewPage(pages) {
    let color = "var(--normal-text-color)"
    if (this.getLevel(pages) == 0)
      color = "var(--tag-text-color)"
    return color
  }
  
  flashcardLink(pages) {
    const align = "left"
    const color = this.tagColorForNewPage(pages)
    const path = pages.file.path + "#" + pages["View"]

    let link = `<div style="display: inline-block; text-align: ${align}; width: 90%;"><p><span>`

    if (pages.Front instanceof Array) {
      pages.Front.forEach(p => {
        link += `<a style="color: ${color};" data-tooltip-position="top" data-href=${path} href=${path} class="internal-link" target="_blank" rel="noopener">`
        link += p
        link += `</a>`
        link += "<br>"
      })
    }
    else {
      link += `<a style="color: ${color};" data-tooltip-position="top" data-href=${path} href=${path} class="internal-link" target="_blank" rel="noopener">`
      link += pages.Front
      link += `</a>`
    }
    link += `</span></p></div>`

    return link
  }
}