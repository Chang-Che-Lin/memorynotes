class MemoryApi {
  // answer state
  showAnswerState(dv, current) {
    const { MemoryFunc } = customJS

    return (MemoryFunc.answerState(dv, current))
  }

  flashcardSpacedRepetition(dv) {
    const { MemoryFunc } = customJS

    const memoryPagePath = dv.io.normalize("Settings/Spaced repetition.md")
    const memoryPage = dv.page(memoryPagePath)

    const { createButton } = app.plugins.plugins["buttons"]
    const { update } = app.plugins.plugins["metaedit"].api
    // const {postValues} = app.plugins.plugins["metadata-menu"].api

    const today = dv.luxon.DateTime.now().toFormat('yyyy-MM-dd')
    const maxLevel = memoryPage["Level MAX"]

    const currentFile = dv.app.workspace.getActiveFile()

    const correct = async (file) => {
      const currentLevel = dv.current().level?.split("%%")[0]
      const nextLevel = MemoryFunc.levelUp(currentLevel, maxLevel)
      const diffDate = memoryPage[`Level ${nextLevel}`]
      const nextLearnDate = MemoryFunc.offsetDate(dv, today, diffDate)
      const currentLearnDate = dv.current()["Next Learn Date"]?.split("%%")[0]
      // const updateValue = [
      //   {
      //     name: 'Next Learn Date',
      //     payload: {
      //       value: `${nextLearnDate}%%${today}%%`,
      //     } 
      //   },
      //   {
      //     name: 'Level',
      //     payload: {
      //       value: `${nextLevel}%%${currentLevel}%%`,
      //     } 
      //   },
      // ]

      if (MemoryFunc.compareDate(dv, currentLearnDate, today) != 1) {
        await update('Next Learn Date', `${nextLearnDate}%%${today}%%`, file)
        await update('Level', `${nextLevel}%%${currentLevel}%%`, file)
        // await postValues(file, updateValue)
        MemoryFunc.updateDataview(dv, currentFile)
      }
    }

    const miss = async (file) => {
      const currentLevel = dv.current().level?.split("%%")[0]
      const nextLevel = MemoryFunc.levelUp(MemoryFunc.levelReset(), maxLevel)
      const diffDate = memoryPage[`Level ${nextLevel}`]
      const nextLearnDate = MemoryFunc.offsetDate(dv, today, diffDate)
      const currentLearnDate = dv.current()["Next Learn Date"]?.split("%%")[0]
      // const updateValue = [
      //   {
      //     name: 'Next Learn Date',
      //     payload: {
      //       value: `${nextLearnDate}%%${today}%%`,
      //     } 
      //   },
      //   {
      //     name: 'Level',
      //     payload: {
      //       value: `${nextLevel}%%${currentLevel}%%`,
      //     } 
      //   },
      // ]

      if (MemoryFunc.compareDate(dv, currentLearnDate, today) != 1) {
        await update('Next Learn Date', `${nextLearnDate}%%${today}%%`, file)
        await update('Level', `${nextLevel}%%${currentLevel}%%`, file)
        // await postValues(file, updateValue)
        MemoryFunc.updateDataview(dv, currentFile)
      }
    }

    const reselect = async (file) => {
      const pastLevel = dv.current().level?.split("%%")[1]
      const pastLearnDate = dv.current()["Next Learn Date"]?.split("%%")[1]
      // const updateValue = [
      //   {
      //     name: 'Next Learn Date',
      //     payload: {
      //       value: `${today}%%${today}%%`,
      //     } 
      //   },
      //   {
      //     name: 'Level',
      //     payload: {
      //       value: `${pastLevel}%%${pastLevel}%%`,
      //     } 
      //   },
      // ]

      if (MemoryFunc.compareDate(dv, pastLearnDate, today) == 0) {
        await update('Next Learn Date', `${today}%%${today}%%`, file)
        await update('Level', `${pastLevel}%%${pastLevel}%%`, file)
        // await postValues(file, updateValue)
        MemoryFunc.updateDataview(dv, currentFile)
      }
    }

    dv.array([
      createButton({
        app, el: dv.container,
        args: { name: "Correct", class: "button-default" },
        clickOverride: { click: correct, params: [dv.current().file.path] }
      }),
      createButton({
        app, el: dv.container,
        args: { name: "Miss", class: "button-default" },
        clickOverride: { click: miss, params: [dv.current().file.path] }
      }),
      createButton({
        app, el: dv.container,
        args: { name: "Reselect", class: "button-default" },
        clickOverride: { click: reselect, params: [dv.current().file.path] }
      })
    ])
  }

  showProgress(dv) {
    const { MemoryFunc } = customJS

    const dataPages = dv.pages(MemoryFunc.getFlashcardsFolderPath(dv))
    const memoryPagePath = dv.io.normalize("Settings/Spaced repetition.md")
    const memoryPage = dv.page(memoryPagePath)

    const { createButton } = app.plugins.plugins["buttons"]
    const { update } = app.plugins.plugins["metaedit"].api
    // const {postValues} = app.plugins.plugins["metadata-menu"].api
    const arr = [""]

    const totalLevel = MemoryFunc.countTotalLevel(dataPages, memoryPage)
    const currentLevel = MemoryFunc.countCurrentLevel(dataPages)
    const progressbar = MemoryFunc.progressBar(dv, currentLevel, totalLevel)

    const todayFinish = MemoryFunc.countTodayFinish(dv, dataPages)
    const haveFinished = MemoryFunc.countHaveFinished(dv, dataPages)

    const clear = async (dataPages) => {
      const today = dv.luxon.DateTime.now().toFormat('yyyy-MM-dd')
      const level = MemoryFunc.levelReset()
      // const updateValue = [
      //   {
      //     name: 'Next Learn Date',
      //     payload: {
      //       value: `${today}%%${today}%%`,
      //     } 
      //   },
      //   {
      //     name: 'Level',
      //     payload: {
      //       value: `${level}%%${level}%%`,
      //     } 
      //   },
      // ]

      for (let index in dataPages.file.values) {
        await update('Next Learn Date', `${today}%%${today}%%`, dataPages.file.values[index].path)
        await update('Level', `${level}%%${level}%%`, dataPages.file.values[index].path)
        // await postValues(dataPages.file.values[index].path, updateValue)
      }

      MemoryFunc.updateDataview(dv)
    }

    dv.table(
      ["Proficiency", "Total Cards", "Total Finish", "Today Finish", "Clear Records"],
      arr.map(p => [progressbar,
        dataPages.length,
        haveFinished,
        todayFinish,
        createButton({
          app, el: dv.container,
          args: { name: "Clear", class: "button-default" },
          clickOverride: { click: clear, params: [dataPages] }
        })
      ]
      )
    )
  }
  
  showFlashcard(dv) {
    const { MemoryFunc } = customJS

    const dataPages = dv.pages(MemoryFunc.getFlashcardsFolderPath(dv))
    const today = dv.luxon.DateTime.now().toFormat('yyyy-MM-dd')
    let dict = {}

    dataPages.Range.forEach(item => MemoryFunc.countRepeat(item, dict))
    let occurrenceRate = MemoryFunc.countOccurrenceRate(dict, dataPages.length)
    let rankArray = MemoryFunc.rankOccurrenceRate(dict)
    
    dv.table(
      ["Flashcards", "Related Ratio", "Related Ranking"],
      dataPages
        .filter(p => MemoryFunc.compareDate(dv, p["Next Learn Date"]?.split("%%")[0], today) != 1)
        .sort(p => p.file.name, "asc")
        .sort(p => MemoryFunc.getLevel(p), "desc")
        .map(p => [MemoryFunc.flashcardLink(p), occurrenceRate[p.Range] + "%", rankArray[p.Range]])
    )
  }

  makeNewTemplate(dv, template, buttonName) {
    const { MemoryFunc } = customJS

    const { createButton } = app.plugins.plugins["buttons"]

    const addNew = async (file) => {
      const tp = app.plugins.plugins['templater-obsidian'].templater.functions_generator.internal_functions
      const specifyTemplate = await tp.modules_array[1].static_functions.get('find_tfile')(template)
      const newFile = await tp.modules_array[1].static_functions.get('create_new')(specifyTemplate)

      MemoryFunc.updateDataview(dv, newFile)
    }

    dv.array([
      createButton({
        app, el: dv.container,
        args: { name: buttonName, class: "button-default" },
        clickOverride: { click: addNew, params: [dv.current().file.path] }
      })
    ])
  }

  listReviewPage(dv) {
    const reviewPagePath = dv.app.workspace.getActiveFile().parent.path
    const reviewPage = dv.pages(`"` + reviewPagePath + `"`)

    dv.list(
      reviewPage
        .filter(p => p.Type == "Deck")
        .sort(p => p.file.ctime, "asc")
        .map(p => ("[[" + p.file.path + "|" + p.file.path?.split(reviewPagePath + "/")[1]?.split("/")[0] + "]]"))
    )
  }

}